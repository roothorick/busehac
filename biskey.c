/* 
 * BUSEHAC - Encrypted Switch NAND partition driver for Linux
 * Copyright (C) 2019 roothorick
 *
 * BUSEHAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BUSEHAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BUSEHAC.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "biskey.h"

#include "aes.h"
#include "globals.h"
#include "minIni.h"

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

// ishex(), hextoi(), parse_hex_key() taken from hactool © SciresM
static int ishex(char c) {
	if ('a' <= c && c <= 'f') return 1;
	if ('A' <= c && c <= 'F') return 1;
	if ('0' <= c && c <= '9') return 1;
	return 0;
}

static char hextoi(char c) {
	if ('a' <= c && c <= 'f') return c - 'a' + 0xA;
	if ('A' <= c && c <= 'F') return c - 'A' + 0xA;
	if ('0' <= c && c <= '9') return c - '0';
	return 0;
}

static bool parse_hex_key(unsigned char *key, const char *hex, unsigned int len) {
	if (strlen(hex) != 2 * len) {
		//fprintf(stderr, "Key (%s) must be %"PRIu32" hex digits!\n", hex, 2 * len);
		return false;
	}

	for (unsigned int i = 0; i < 2 * len; i++) {
		if (!ishex(hex[i])) {
			//fprintf(stderr, "Key (%s) must be %"PRIu32" hex digits!\n", hex, 2 * len);
			return false;
		}
	}

	memset(key, 0, len);

	for (unsigned int i = 0; i < 2 * len; i++) {
		char val = hextoi(hex[i]);
		if ((i & 1) == 0) {
			val <<= 4;
		}
		key[i >> 1] |= val;
	}
	return true;
}

#define DEFAULT_KEYFILE "/.switch/prod.keys"

char* default_keyfile_path() {
	char* rel_keyfile;
	char* homedir = getenv("HOME");
	if(homedir == NULL) return NULL;
	
	rel_keyfile = malloc( strlen(homedir) + strlen(DEFAULT_KEYFILE) + 1 );
	if(rel_keyfile == NULL) return NULL;
	strcpy(rel_keyfile, homedir);
	strcat(rel_keyfile, DEFAULT_KEYFILE);

	return rel_keyfile;
}


int biskey_load(const int idx, const char* keyfile) {
	char key_octets[2*KEY_SIZE+1]; // +1 for trailing null
	unsigned char key[32];
	
	char* default_keyfile = NULL;
	
	if(keyfile == NULL) {
		default_keyfile = default_keyfile_path();
		keyfile = default_keyfile;
	}
	
	if(keyfile == NULL) return 1;
	
	static char biskey_string[11] = "bis_key_XX";
	sprintf(&biskey_string[8], "%02u", idx);
	
	ini_gets("", biskey_string, "", key_octets, 2*KEY_SIZE+1, keyfile);
	
	free(default_keyfile);
	
	bool ret_b = parse_hex_key(key, key_octets, KEY_SIZE);
	if( !ret_b ) return 1;
	
	aes_ctx = new_aes_ctx(key, KEY_SIZE, AES_MODE_XTS);
	if(aes_ctx == NULL) return 1;
	return 0;
}
