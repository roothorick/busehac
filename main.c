/* 
 * BUSEHAC - Encrypted Switch NAND partition driver for Linux
 * Copyright (C) 2019 roothorick
 *
 * BUSEHAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BUSEHAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BUSEHAC.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "hactool/aes.h"
#include "buse.h"
#include "globals.h"
#include "busehac_ops.h"
#include "biskey.h"

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <errno.h>
#include <string.h>

void version() {
	puts (
		"BUSEHAC prealpha\n"
		"Copyright (C) 2019 roothorick\n"
		"License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\n"
		"This is free software: you are free to change and redistribute it.\n"
		"There is NO WARRANTY, to the extent permitted by law.\n"
		"\n"
		"Based on BUSE (C) 2013 Adam Cozzette\n"
		"Uses crypto helpers from hactool (C) 2018 SciresM\n"
		"Uses minIni (C) 2008-2017 CompuPhase\n"
		"Uses mbedtls (C) 2006-2015 ARM Limited\n"
	);
}

void usage() {
	puts (
		"BUSEHAC NAND mounting tool (c) roothorick\n"
		"\n"
		"Usage: busehac [options] <biskey index> <device/file to mount> <NBD device>\n"
		"\n"
		"Example:\n"
		"	busehac 2 user.enc /dev/nbd3\n"
		"Will mount the encrypted filesystem in user.enc using biskey 2 to /dev/nbd3\n"
		"\n"
		"Options:\n"
		"	--keyfile	File to parse biskeys from (default ~/.switch/prod.keys)\n"
		"	--version	Print version and copyright info, then exit\n"
	);
}

const struct option CMDLINE_OPTIONS[] = {
	{ "keyfile", 1, NULL, 1 },
	{ "version", 0, NULL, 2 },
	{ NULL, 0, NULL, 0 }
};

int main(int argc, char* argv[]) {
	if(argc < 4) {
		usage();
		exit(1);
	}
	
	const char* keyfile = NULL;
	
	while(1) {
		int opt_ret = getopt_long(argc, argv, "", CMDLINE_OPTIONS, NULL);
		
		if(opt_ret == -1) break;
		
		switch( opt_ret ) {
			case '?':
				// getopt_long() already printed an error
				puts("\n");
				usage();
				exit(1);
				break;
			case 1: // keyfile
				keyfile = optarg;
				break;
			case 2: // version
				version();
				exit(0);
				break;
		}
	}
	
	unsigned int biskey_idx = atoi(argv[optind]);
	const char* partition_file = argv[optind+1];
	const char* nbd_file = argv[optind+2];
	
	biskey_load(biskey_idx, keyfile);
	if( aes_ctx == NULL ) {
		puts("Failed to create AES context?\n");
		exit(1);
	}
	
	partition_fd = fopen(partition_file, "r+");
	if(partition_fd == NULL) {
		printf("Failed to open partition: %s\n", strerror(errno) );
		exit(1);
	}
	
	int ret = fseek(partition_fd, 0, SEEK_END);
	if(ret != 0) {
		printf("Could not find end of partition: %s\n", strerror(errno) );
		exit(1);
	}
	
	// No need to reset the file pointer; busehac_ops fns will re-seek as needed
	
	// The encryption can only work in whole 0x4000 blocks. Ignore any trailing partial block.
	u_int64_t real_file_size = ftell(partition_fd);
	busehac_ops.size_blocks = (real_file_size - real_file_size % BLOCK_SIZE) / 0x200;
	
	return buse_main(nbd_file, &busehac_ops, NULL);
}
