/* 
 * BUSEHAC - Encrypted Switch NAND partition driver for Linux
 * Copyright (C) 2019 roothorick
 *
 * BUSEHAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BUSEHAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BUSEHAC.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "globals.h"
#include "busehac_ops.h"
#include "hactool/aes.h"
#include "biskey.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define READ_OFFSET 0x2a
#define READ_SIZE 0x8054

unsigned char testbuf[READ_SIZE];
unsigned char unencrypted_buf[READ_SIZE];

// DOES NOT RETURN
void bail(unsigned char* error) {
	puts(error);
	exit(1);
}

int main() {
	int ret = biskey_load(0, "./data/test.keys");
	if(ret != 0) bail("Failed to load test biskey\n");
	
	partition_fd = fopen("./data/testfs.enc", "r");
	if(partition_fd == NULL) bail("Failed to open encrypted FAT file\n");
	
	// Fits one whole block, but begins and ends reading at weird places to cover all codepaths
	ret = busehac_read(testbuf, READ_SIZE, READ_OFFSET, NULL);
	if(ret != 0) bail("busehac_read() failed\n");
	
	fclose(partition_fd); // Ignore failures
	
	FILE* unencrypted_fd = fopen("./data/testfs.fat", "r");
	if(unencrypted_fd == NULL) bail("Failed to open unencrypted FAT file\n");
	
	ret = fseek(unencrypted_fd, READ_OFFSET, SEEK_SET);
	if(ret != 0) bail("fseek() in unencrypted FAT failed\n");
	ret = fread(unencrypted_buf, 1, READ_SIZE, unencrypted_fd);
	if(ret != READ_SIZE) bail("fread() in unencrypted FAT failed\n");
	
	fclose(unencrypted_fd); // Ignore failures
	
	ret = memcmp(testbuf, unencrypted_buf, READ_SIZE);
	if(ret != 0) bail("Decrypted data does not match unencrypted FAT\n");
	
	printf("Read test PASSED\n");
	exit(0);
}
