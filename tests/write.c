/* 
 * BUSEHAC - Encrypted Switch NAND partition driver for Linux
 * Copyright (C) 2019 roothorick
 *
 * BUSEHAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BUSEHAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BUSEHAC.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "globals.h"
#include "busehac_ops.h"
#include "hactool/aes.h"
#include "biskey.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TOTAL_WRITE_SIZE 0xC000

#define FIRST_WRITE_SIZE 0x2a
#define SECOND_WRITE_SIZE 0x8054
#define THIRD_WRITE_SIZE TOTAL_WRITE_SIZE - SECOND_WRITE_SIZE - FIRST_WRITE_SIZE

unsigned char testbuf[TOTAL_WRITE_SIZE];
unsigned char unencrypted_buf[TOTAL_WRITE_SIZE];

// DOES NOT RETURN
void bail(unsigned char* error) {
	puts(error);
	exit(1);
}

int main() {
	int ret = biskey_load(0, "./data/test.keys");
	if(ret != 0) bail("Failed to load test biskey\n");
	
	partition_fd = fopen("./data/scratch", "w+");
	if(partition_fd == NULL) bail("Failed to open scratchfile\n");
	
	// BUSEHAC assumes the file/device is already the correct size
	fseek(partition_fd, TOTAL_WRITE_SIZE, SEEK_SET);
	fputc(0x0, partition_fd);
	
	memset(unencrypted_buf, 0x42, TOTAL_WRITE_SIZE);
	
	// Covering bytes before the weird write
	ret = busehac_write(unencrypted_buf, FIRST_WRITE_SIZE, 0, NULL);
	if(ret != 0) bail("First busehac_write() failed\n");
	// Fits one whole block, but begins and ends writing at weird places to cover all codepaths
	ret = busehac_write(unencrypted_buf, SECOND_WRITE_SIZE, FIRST_WRITE_SIZE, NULL);
	if(ret != 0) bail("Second busehac_write() failed\n");
	// Covering bytes after the weird write
	ret = busehac_write(unencrypted_buf, THIRD_WRITE_SIZE, FIRST_WRITE_SIZE + SECOND_WRITE_SIZE, NULL);
	if(ret != 0) bail("Third busehac_write() failed\n");
	
	// Just in case
	ret = busehac_flush(NULL);
	if(ret != 0) bail("busehac_flush() failed\n");
	
	// Now read it all back
	ret = busehac_read(testbuf, TOTAL_WRITE_SIZE, 0, NULL);
	if(ret != 0) bail("busehac_read() failed during write test\n");
	
	fclose(partition_fd); // Ignore failures
	
	ret = memcmp(testbuf, unencrypted_buf, TOTAL_WRITE_SIZE);
	if(ret != 0) bail("Data read back from scratchfile does not match data written\n");
	
	printf("Write test PASSED\n");
	exit(0);
}
