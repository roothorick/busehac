/* 
 * BUSEHAC - Encrypted Switch NAND partition driver for Linux
 * Copyright (C) 2019 roothorick
 *
 * BUSEHAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BUSEHAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BUSEHAC.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef BUSEHAC_OPS_H
#define BUSEHAC_OPS_H

#include "buse.h"

extern struct buse_operations busehac_ops;

int busehac_read(void *_buf, u_int32_t len, u_int64_t offset, void *userdata);
int busehac_write(const void *_buf, u_int32_t len, u_int64_t offset, void *userdata);
void busehac_disc(void *userdata);
int busehac_flush(void *userdata);
int busehac_trim(u_int64_t from, u_int32_t len, void *userdata);
	
#endif
