/* 
 * BUSEHAC - Encrypted Switch NAND partition driver for Linux
 * Copyright (C) 2019 roothorick
 *
 * BUSEHAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BUSEHAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BUSEHAC.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef BUSEHAC_GLOBALS_H
#define BUSEHAC_GLOBALS_H

#include "hactool/aes.h"
#include "buse.h"

#include <stdio.h>

#define BLOCK_SIZE 0x4000
#define KEY_SIZE 32

extern aes_ctx_t* aes_ctx;
extern FILE* partition_fd;

#endif
