/* 
 * BUSEHAC - Encrypted Switch NAND partition driver for Linux
 * Copyright (C) 2019 roothorick
 *
 * BUSEHAC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BUSEHAC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BUSEHAC.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "busehac_ops.h"

#include "aes.h"
#include "globals.h"

#include <stdio.h>
#include <string.h>

char workbuffers[2][BLOCK_SIZE]; // Encrypted in 0, decrypted in 1

struct buse_operations busehac_ops = {
	.read = &busehac_read,
	.write = &busehac_write,
	.disc = &busehac_disc,
	.flush = &busehac_flush,
	.trim = &busehac_trim,
	.blksize = 0x200
	// size_blocks will be filled in by main()
};

int busehac_read(void *_buf, u_int32_t len, u_int64_t offset, void *userdata) {
	unsigned char* buf = (unsigned char*) _buf; // So that we can do pointer math

	// Sanity check
	if(len == 0) return 0;
	
	int ret;
	
	u_int64_t starting_block = offset / BLOCK_SIZE;
	u_int64_t rel_offt = offset - starting_block * BLOCK_SIZE;
	
	// if len+rel_offt % BLOCK_SIZE != 0, then len / BLOCK_SIZE will be 1 less than needed
	// if len+rel_offt % BLOCK_SIZE == 0, then len / BLOCK_SIZE will be correct
	u_int32_t num_blocks = (len+rel_offt-1) / BLOCK_SIZE + 1;
	
	ret = fseek(partition_fd, starting_block * BLOCK_SIZE, SEEK_SET);
	if(ret != 0) return ret;
	
#ifndef RELEASE
	printf("DBG read: offset %lx len %x rel_offt %lx starting_block %lu num_blocks %u\n",
		offset, len, rel_offt, starting_block, num_blocks);
#endif
	
	for(u_int32_t i=0; i<num_blocks; i++) {		
		ret = fread(workbuffers[0], BLOCK_SIZE, 1, partition_fd);
		if(ret != 1) return 1;
		
		u_int32_t write_offset;
		if(i==0)
			write_offset = 0;
		else
			write_offset = i * BLOCK_SIZE - rel_offt;
		
		u_int32_t write_len;
		
		if(i==0) {
			if(len + rel_offt < 0x4000)
				write_len = len;
			else
				write_len = 0x4000 - rel_offt;
		}
		else
			write_len = len - write_offset;
		
		if(write_len > 0x4000)
			write_len = 0x4000;
		
#ifndef RELEASE
		printf("DBG read block: write_offset %x write_len %x\n", write_offset, write_len);
#endif
		
		unsigned char* write_location = buf + write_offset;
		
		// For whole blocks, skip the second workbuffer to speed things up
		if(write_len == 0x4000) {
			aes_xts_decrypt(aes_ctx, write_location, workbuffers[0], BLOCK_SIZE, starting_block+i, BLOCK_SIZE);
		}
		else
		{
			aes_xts_decrypt(aes_ctx, workbuffers[1], workbuffers[0], BLOCK_SIZE, starting_block+i, BLOCK_SIZE);
			if(i==0) memcpy(write_location, workbuffers[1]+rel_offt, write_len);
			else memcpy(write_location, workbuffers[1], write_len);
		}
	}
	
	return 0;
}

int busehac_write(const void *_buf, u_int32_t len, u_int64_t offset, void *userdata) {
	const unsigned char* buf = (const unsigned char*) _buf; // So that we can do pointer math
	
	// Sanity check
	if(len == 0) return 0;
	
	int ret;
	
	u_int64_t starting_block = offset / BLOCK_SIZE;
	u_int64_t rel_offt = offset - starting_block * BLOCK_SIZE;
	
	// if len+rel_offt % BLOCK_SIZE != 0, then len / BLOCK_SIZE will be 1 less than needed
	// if len+rel_offt % BLOCK_SIZE == 0, then len / BLOCK_SIZE will be correct
	u_int32_t num_blocks = (len+rel_offt-1) / BLOCK_SIZE + 1;

	ret = fseek(partition_fd, starting_block * BLOCK_SIZE, SEEK_SET);
	if(ret != 0) return ret;

#ifndef RELEASE
	printf("DBG write: offset %lx len %x rel_offt %lx starting_block %lu num_blocks %u\n",
		offset, len, rel_offt, starting_block, num_blocks);
#endif

	for(u_int32_t i=0; i<num_blocks; i++) {
		u_int32_t read_offset;
		if(i==0)
			read_offset = 0;
		else
			read_offset = i * BLOCK_SIZE - rel_offt;
		
		u_int32_t read_len;
		
		if(i==0) {
			if(len + rel_offt < 0x4000)
				read_len = len;
			else
				read_len = 0x4000 - rel_offt;
		}
		else
			read_len = len - read_offset;
		
		if(read_len > 0x4000)
			read_len = 0x4000;
		
		const unsigned char* read_location = buf + read_offset;
		
#ifndef RELEASE
	printf("DBG write block: read_offset %x read_len %x\n", read_offset, read_len);
#endif
		// For whole blocks, we don't need to read, as we already have the entire block to be encrypted.
		// Also, skip the second workbuffer to speed things up
		if(read_len == 0x4000) {
			aes_xts_encrypt(aes_ctx, workbuffers[0], read_location, BLOCK_SIZE, starting_block+i, BLOCK_SIZE);
		}
		else
		{
			ret = fread(workbuffers[0], BLOCK_SIZE, 1, partition_fd);
			if(ret != 1) return 1;
			ret = fseek(partition_fd, -BLOCK_SIZE, SEEK_CUR);
			if(ret != 0) return ret;
			
			aes_xts_decrypt(aes_ctx, workbuffers[1], workbuffers[0], BLOCK_SIZE, starting_block+i, BLOCK_SIZE);
			
			if(i==0) memcpy(workbuffers[1]+rel_offt, read_location, read_len);
			else memcpy(workbuffers[1], read_location, read_len);
			
			aes_xts_encrypt(aes_ctx, workbuffers[0], workbuffers[1], BLOCK_SIZE, starting_block+i, BLOCK_SIZE);
		}
		
		ret = fwrite(workbuffers[0], BLOCK_SIZE, 1, partition_fd);
	}
	
	return 0;
}

void busehac_disc(void *userdata) {
#ifndef RELEASE
	puts("DBG disconnect");
#endif
	// XXX: There is no way to report an error!
	int ret = fclose(partition_fd);
	if(ret != 0) printf("WARNING: fclose() failed. There may be data corruption.\n");
}

int busehac_flush(void *userdata) {
#ifndef RELEASE
	puts("DBG flush");
#endif
	return fflush(partition_fd);
}

int busehac_trim(u_int64_t from, u_int32_t len, void *userdata) {
#ifndef RELEASE
	puts("DBG trim");
#endif
	// No-op
	return 0;
}
