INCLUDE = -I . -I ./buse -I ./hactool -I ./hactool/mbedtls/include -I ./minIni/dev
CFLAGS = -std=gnu11 -MMD -Wunused
LDFLAGS = -lm -lmbedcrypto -L hactool/mbedtls/library
CC = gcc
AR = ar
MAIN_EXE = busehac
SOURCES = biskey.c busehac_ops.c globals.c buse/buse.c hactool/aes.c minIni/dev/minIni.c
MAIN_EXE_SOURCES = main.c
OBJS = $(SOURCES:%.c=build/%.o)
MAIN_EXE_OBJS = $(MAIN_EXE_SOURCES:%.c=build/%.o)
TESTS = read write
TEST_EXES = $(TESTS:%=tests/bin/%)

ifdef RELEASE
CFLAGS += -O3
else
CFLAGS += -O0 -ggdb -Werror
endif

all: $(MAIN_EXE)

# XXX: For whatever reason .PHONY is doing precisely the opposite of what the documentation says. Falling back on .FORCE
# .PHONY: tests $(TEST_EXES)
.FORCE:

tests: $(TEST_EXES) .FORCE

tests/bin/%: $(OBJS) .FORCE
	@mkdir -p $(dir $@)
	$(CC) -o $@ $(@:tests/bin/%=tests/%.c) $(OBJS) $(INCLUDE) $(CFLAGS) $(LDFLAGS)
	cd tests && $(@:tests/bin/%=bin/%)

mbedtls:
	cd hactool/mbedtls && $(MAKE) lib

clean:
	rm -rf $(MAIN_EXE) tests/bin build

$(MAIN_EXE): mbedtls $(OBJS) $(MAIN_EXE_OBJS)
	$(CC) -o $@ $(OBJS) $(MAIN_EXE_OBJS) $(LDFLAGS)

build/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(INCLUDE) -c -o $@ $<

-include $(OBJS:.o=.d)
